// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FPS_ExampleGameMode.generated.h"

UCLASS(minimalapi)
class AFPS_ExampleGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFPS_ExampleGameMode();
};



