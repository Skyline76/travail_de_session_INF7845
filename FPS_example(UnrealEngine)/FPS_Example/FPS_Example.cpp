// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "FPS_Example.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, FPS_Example, "FPS_Example" );
 