// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "FPS_ExampleGameMode.h"
#include "FPS_ExampleHUD.h"
#include "FPS_ExampleCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFPS_ExampleGameMode::AFPS_ExampleGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFPS_ExampleHUD::StaticClass();
}
