# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Client UI
module jumpngun is
	app_name "Jump n' gun"
	app_namespace "org.gamnit.jumpngun"
	app_version(0, 1, git_revision)
	android_api_target 10
end

import gamnit::tileset
import gamnit::depth
import gamnit::keys
import gamnit::bmfont
import app::audio

import game

redef class App

	# Current world logic
	fun level: Level do return context.level

	# Current game context
	var context: nullable GameContext = null

	# ---
	# Sound effect, lazy loaded at first use

	private var shoot_sound = new Sound("sounds/shoot.ogg")
	private var shoot_turret_sound = new Sound("sounds/shoot_turret.ogg")
	private var landing_sound = new Sound("sounds/landing.ogg")
	private var jetpack_sound = new Sound("sounds/jetpack.ogg")
	private var jetpack_low_sound = new Sound("sounds/jetpack_low.ogg")
	private var jump_sound = new Sound("sounds/jump.ogg")
	private var fall_sound = new Sound("sounds/fall.ogg")
	private var victory_sound = new Sound("sounds/victory.ogg")

	# Explosion, close (index 0) to far (2)
	private var explosion_sounds: Array[Sound] = [
		new Sound("sounds/explosion0.ogg"),
		new Sound("sounds/explosion1.ogg"),
		new Sound("sounds/explosion2.ogg")]

	# ---
	# 3D models

	private var model_gun = new Model("models/gun.obj")
	private var model_bullet = new Model("models/bullet.obj")
	private var model_gun_turret_head = new Model("models/turret_head.obj")
	private var model_gun_turret_base = new Model("models/turret_base.obj")
	private var model_shield_turret_base = new Model("models/shield_turret_base.obj")
	private var model_shield_turret_heads = new Array[Model].with_items(
		new Model("models/shield_turret_head0.obj"),
		new Model("models/shield_turret_head1.obj"),
		new Model("models/shield_turret_head2.obj"))

	private var model_player = new Model("models/player.obj")

	private var model_axes = new Model("models/axes_arrows.obj")

	# ---
	# Always present sprites and actors

	private var crosshair_texture = new Texture("textures/crosshair.png")

	private var fuel_unit_texture = new Texture("textures/fuel_unit.png")
	private var fuel_sprites = new Array[Sprite]

	# ---
	# Particle effects (textures and systems)

	private var texture_explosion = new Texture("particles/explosion00.png")
	private var texture_smoke = new Texture("particles/blackSmoke12.png")
	private var explosion_system = new ParticleSystem(20, explosion_program, texture_explosion)
	private var smoke_system = new ParticleSystem(100, smoke_program, texture_smoke)

	# ---
	# Font and text

	# Pretty font
	var font = new BMFontAsset("font/Wendy.fnt")

	private var text = new TextSprites(font, ui_camera.center.offset(0.0, -128.0, 0.0), "", align=0.5) is lazy

	redef fun create_scene
	do
		texture_explosion.root.premultiply_alpha = false

		super

		var display = display
		assert display != null

		# Load the font
		font.load
		var error = font.error
		if error != null then print_error error

		# Load models
		for model in models do model.load

		# Hide mouse and lock cursor in window for an FPS look
		display.show_cursor = false
		display.lock_cursor = true

		# Setup display
		glClearColor(0.4, 0.7, 1.0, 1.0)
		gl.capabilities.cull_face.enable

		# Register the two particle systems
		particle_systems.add explosion_system
		particle_systems.add smoke_system

		# Setup the level fuel indicator
		var n_units = 16
		for i in n_units.times do
			var x = 36.0 * (i.to_f-(n_units.to_f*0.5))
			var y = -80
			var s = new Sprite(fuel_unit_texture, ui_camera.top.offset(x, y, 0.0))
			fuel_sprites.add s
			s.scale = 0.5
			s.red = 0.0
			s.blue = 0.0
			s.green = 0.8
			ui_sprites.add s
		end

		# Ground as a sphere
		var sphere = new UVSphere(1000.0, 64, 32)
		var black = [0.0, 0.0, 0.0, 1.0]
		var sphere_material = new SmoothMaterial(black, black, black)
		actors.add new Actor(new LeafModel(sphere, sphere_material), new Point3d[Float](y=-1100.0))

		# Sun as a sphere
		sphere = new UVSphere(10.0, 64, 32)
		var light_yellow = [1.0, 0.95, 0.4, 1.0]
		sphere_material = new SmoothMaterial(light_yellow, [0.0]*4, [0.0]*4)
		actors.add new Actor(new LeafModel(sphere, sphere_material), light.position)

		# Generate world
		new_level

		# Configure the sun
		var sun_light = new ParallelLight
		sun_light.pitch = 0.25*pi
		sun_light.yaw = 0.35*pi
		sun_light.width = 250.0
		sun_light.height = 250.0
		app.light = sun_light

		# Place the crosshair at the center of the UI
		var crosshair = new Sprite(crosshair_texture, ui_camera.center.offset(0.0, 0.0, 0.0))
		ui_sprites.add crosshair
		crosshair.scale = 4.0

		# Show tutorial text
		text.text = """
Reach the gold tower!

Move with WASD,
jump with space and
shoot with LMB.
"""
	end

	# Load a new level, spawn player and clear old level if needed
	fun new_level
	do
		var context = self.context
		if context == null then
			# First level
			context = new_game_context
			self.context = context
		else
			context.new_level
		end
	end

	redef fun update(dt)
	do
		var context = context
		assert context != null
		var player = context.player

		var speed
		if player.air then
			# In free fall
			speed = 0.25
		else
			# On the ground
			speed = 12.0
		end

		for key in app.pressed_keys do
			if key == "z" then
				context.player.speed.move(0.0, 0.0, speed, world_camera)
				has_moved = true
			else if key == "s" then
				context.player.speed.move(0.0, 0.0, -speed, world_camera)
				has_moved = true
			else if key == "q" then
				context.player.speed.move(-speed, 0.0, 0.0, world_camera)
				has_moved = true
			else if key == "d" then
				context.player.speed.move(speed, 0.0, 0.0, world_camera)
				has_moved = true
			else if key == "space" then
				context.player.activate_jetpack dt
			end
		end

		context.player.gun.orientation.fill_from world_camera.orientation

		# Update game logic
		context.update dt

		# Update camera pos
		world_camera.position.fill_from context.player.position
		context.player.update_ui

		# Update fuel level
		var l = (fuel_sprites.length.to_f*player.fuel.to_f/player.max_fuel.to_f).to_i
		for i in [0..l[ do
			fuel_sprites[i].alpha = 1.0
		end

		# Update fuel indicator
		for i in [l..fuel_sprites.length[ do
			fuel_sprites[i].alpha = 0.2
		end

		# Close the tutorial if its complete
		if tutorial_open and tutorial_complete then
			text.text = ""
			tutorial_open = false
		end

	end

	redef fun accept_event(event)
	do
		if super then return true

		if event isa QuitEvent or
		  (event isa KeyEvent and event.name == "escape" and event.is_up) then
			# When window close button, escape or back key is pressed
			# show the average FPS over the last few seconds.
			print "{current_fps} fps"
			print sys.perfs

			# Quit abruptly
			exit 0
		end

		var context = context
		if context != null then
			if event isa KeyEvent and event.is_down and event.name == "space" and not context.player.air then
				context.player.jump
				has_jumped = true
			else if event isa KeyEvent and not event.is_down and event.name == "return" then
				if not context.player.alive then
					# Dead, restart!
					context.spawn_player
					text.text = ""
				else if context.player.won then
					# Won, load new level
					new_level
					text.text = ""
				end
			else if event isa PointerEvent and not event.is_move and event.pressed then
				context.player.try_to_fire
				has_fired = true
				return true
			end
		end

		return false
	end

	# ---
	# Tutorial

	# Has the player used all features presented in the tutorial?
	fun tutorial_complete: Bool do return has_jumped and has_moved and has_fired

	# Is the tutorial currently open?
	var tutorial_open = true

	# Has the player jumped once?
	var has_jumped = false

	# Has the player moved once?
	var has_moved = false

	# Has the player opened fire once?
	var has_fired = false
end

redef class Point3d[N]
	# Move this point on X and Z considering the orientation of `camera`
	fun move(dx, dy, dz: Float, camera: EulerCamera)
	do
		assert self isa Point3d[Float]

		# +dz move forward
		x -= camera.yaw.sin*dz
		z -= camera.yaw.cos*dz

		# +dx strafe to the right
		x += camera.yaw.cos*dx
		z -= camera.yaw.sin*dx
	end
end

redef class EulerCamera
	# Directional vector of the current camera orientation
	#
	# Clients should not modify the returned point, it is cached.
	fun orientation: Point3d[Float]
	do
		var r = orientation_cache
		r.from_euler_angles(pitch, yaw)
		return r
	end

	private var orientation_cache = new Point3d[Float]
end

redef class Level
	redef fun explode(at, force)
	do
		super

		# Particles effect
		app.explosion_system.add(at, 4096.0, 0.3)
		for i in 8.times do
			app.explosion_system.add(
				new Point3d[Float](at.x & 1.0, at.y & 1.0, at.z & 1.0),
				2048.0 & 1024.0, 0.3 & 0.1)
		end

		# Smoke
		for s in 24.times do
			var dt = 0.2 * s.to_f + 0.1.rand
			app.smoke_system.add(
				new Point3d[Float](at.x & 0.2, at.y & 0.2, at.z & 0.2),
				1024.0 & 512.0, 10.0 & 4.0, dt)
		end
	end
end

redef class GameContext

	private var landing_cooldown = 0.0
	redef fun update(dt)
	do
		var was_air = player.air
		super

		if was_air and not player.air and level.physic_entities.has(player) then
			if landing_cooldown <= 0.0 then
				app.landing_sound.play
				landing_cooldown = 0.4
			end
		end
		landing_cooldown -= dt
	end

	redef fun spawn_player
	do
		super

		# Spawn by looking at the goal
		var yaw = level.spawn_tower.platform_center.atan2_xz(level.goal_tower.platform_center)
		app.world_camera.yaw = yaw
	end

	redef fun cleanup_old_level(old_level)
	do
		for t in level.towers do app.actors.remove t.actor
		for pe in level.physic_entities.to_a do pe.destroy
	end

	redef fun install_new_level(level)
	do
		spawn_player
		prepare

		#var axes = new Actor(app.model_axes, new Point3d[Float])
		#axes.center.fill_from player.position
		#app.actors.add axes
	end
end

redef class Tower

	# UI actor of this tower
	var actor = new Actor(model, platform_center.offset(0.0, -0.5*height, 0.0)) is lazy

	var model: Model = new LeafModel(to_mesh, material) is lazy

	private fun material: Material
	do
		if goal then
			var r = 1.0
			var g = 0.8
			var b = 0.3
			return new SmoothMaterial([r, g, b, 1.0], [r, g, b, 1.0], [0.0]*4)
		else
			var c = 0.2 & 0.1
			return new SmoothMaterial([c, c, c, 1.0], [c, c, c, 1.0], [0.0]*4)
		end
	end

	redef fun create_ui do app.actors.add actor
end

redef class Bullet

	# UI actor of this bullet
	var actor = new Actor(model, position) is lazy

	private fun model: Model is abstract

	redef fun create_ui
	do
		var o = new Point3d[Float]
		actor.pitch = o.pitch(gun.orientation)
		actor.yaw = o.atan2_xz(gun.orientation)
		actor.scale = 25.0
		app.actors.add actor
	end

	redef fun destroy
	do
		super
		app.actors.remove actor
	end
end

redef class Gun
	redef fun open_fire(shooter)
	do
		# Play a sound if the shooter is the local player
		var context = app.context
		if context != null and shooter == context.player then
			app.shoot_sound.play
		else app.shoot_turret_sound.play

		return super
	end
end

redef class ExplosiveBullet
	redef fun model do return app.model_bullet

	redef fun hit(entity, pos)
	do
		super

		var context = app.context
		if context == null then return

		var d = context.player.head.dist(pos)
		d = d & 2.0
		var i = if d < 5.0 then
				0
			else if d < 50.0 then
				1
			else 2

		app.explosion_sounds[i].play
	end
end

redef class TurretPart

	# UI actor of this bullet
	var actor = new Actor(model, position) is lazy

	private fun model: Model is abstract

	redef fun create_ui
	do
		super
		app.actors.add actor
		actor.scale = 0.5
	end

	redef fun destroy
	do
		super
		app.actors.remove actor
	end
end

redef class GunTurretHead
	redef fun model do return app.model_gun_turret_head

	redef fun update(dt)
	do
		super
		actor.pitch = pitch
		actor.yaw = yaw
	end
end

redef class GunTurretBase
	redef fun model do return app.model_gun_turret_base

	redef fun create_ui
	do
		super
		actor.yaw = 2.0*pi.rand
	end
end

redef class ShieldTurret

	# UI actor for the translucent shield
	private var shield: Actor is lazy do
		var sphere = new UVSphere(shield_radius, 16, 12)
		var blue =  [0.0*0.2, 1.0*0.2, 1.0*0.2, 0.1]
		var blue1 = [1.0*0.5, 1.0*0.5, 1.0*0.5, 0.1]
		var spec =  [1.0, 1.0,     1.0,     0.1]
		var sphere_material = new SmoothMaterial(blue, blue1, spec)
		var model = new LeafModel(sphere, sphere_material)
		var actor = new Actor(model, floor_center.offset(0.0, 2.0, 0.0))
		actor.yaw = 2.0*pi.rand
		return actor
	end

	redef fun create_shield
	do
		app.actors.add shield
	end

	redef fun active=(value)
	do
		if value != active then app.actors.remove shield
		super
	end
end

redef class ShieldTurretHead
	redef fun model do return app.model_shield_turret_heads[n]

	redef fun update(dt)
	do
		super
		actor.yaw = yaw
	end
end

redef class ShieldTurretBase
	redef fun model do return app.model_shield_turret_base
end

redef class Character
	redef fun destroy
	do
		super

		if not won then
			app.fall_sound.play

			var context = app.context
			if context != null and self == context.player and
			   position.y <= level.death_limit then app.text.text = """
You fell to your death!
Hit enter to respawn."""
		end
	end

	redef fun won=(val)
	do
		if val and not won then
			app.victory_sound.play

			var context = app.context
			if context != null and self == context.player then app.text.text = """
You won!

Hit enter to start a new game.
"""
		end
		super
	end

	# Play the jetpack sound at exactly 0.2 seconds
	private var jetpack_cooldown = 0.0
	redef fun update(dt)
	do
		if jetpack_try_active and jetpack_cooldown <= 0.0 then
			if fuel <= 0.1 then
				app.jetpack_low_sound.play
			else app.jetpack_sound.play
			jetpack_cooldown = 0.2
		end
		jetpack_cooldown -= dt

		super
	end

	redef fun jump
	do
		super
		app.jump_sound.play
	end

	# Color of the player, for `actor`
	fun actor_color: Array[Float] do return [0.0, 0.5, 0.0, 1.0]

	private var actor: Actor is lazy do
		var mesh = app.model_player.leaves.first.mesh
		var color = actor_color
		var sphere_material = new SmoothMaterial(color, color, color)
		return new Actor(new LeafModel(mesh, sphere_material), position)
	end

	private var gun_actor = new Actor(app.model_gun, new Point3d[Float](0.0, 1.0, 0.0)) is lazy

	redef fun create_ui
	do
		super

		app.actors.add actor

		app.actors.add gun_actor
		gun_actor.scale = 7.5
	end

	# Update `actor` and `gun_actor`
	fun update_ui
	do
		var o = once new Point3d[Float]
		var yaw = o.atan2_xz(gun.orientation)
		var pitch = o.pitch(gun.orientation)
		actor.yaw = yaw

		# Move the gun model with the camera
		var r = yaw + 0.8*pi
		var x = 0.2
		var d = 1.0
		gun_actor.center.x = position.x + x*r.sin*d
		gun_actor.center.z = position.z + x*r.cos*d
		gun_actor.center.y = position.y - 0.3

		gun_actor.pitch = pitch*0.75
		gun_actor.yaw = yaw
	end
end
