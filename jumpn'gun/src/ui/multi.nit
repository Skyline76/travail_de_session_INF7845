# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Optional multiplayer support with automated server discovery
module multi

import gamnit::network

intrude import game
intrude import game::physics
intrude import game::guns
import jumpngun

import multi_transients

# Factory to find a server or create one
redef fun new_game_context
do
	# Discover a local server
	var servers = discover_local_servers
	if servers.not_empty then
		var server_info = servers.first
		var server = new RemoteServer(server_info)

		if not server.connect then
			app.network_info = "Failed to connect to {server_info.address}:{server_info.port}"
		else if not server.handshake then
			app.network_info = "Failed handshake with {server_info.address}:{server_info.port}"
		else
			app.network_info = "Connected to {server_info.address}:{server_info.port}"

			# Connection is good, read level
			var level = server.reader.deserialize
			assert level isa Level else print level or else "null"

			var context = new ClientContext(level, server)
			server.writer.serialize context.player
			return context
		end
	end

	# Create our own server
	var connect_port = 38271
	app.network_info = "Launching server: connect on {connect_port}, discovery on {discovery_port}"
	var server = new Server(connect_port)
	server.answer_discovery_requests
	server.accept_clients

	var level = new Level
	level.generate
	return new ServerContext(level, server)
end

redef class GameContext

	# Share `command` with other players over the network
	fun share(command: Serializable) do end
end

# Client or server game context, with a shared `level`
abstract class NetworkContext
	super GameContext

	redef fun update(dt)
	do
		super
		share player.to_position_command
	end
end

# Server, running a game locally and hosting clients via `server`
class ServerContext
	super NetworkContext

	# Server logic, manages the clients
	var server: Server

	redef fun update(dt)
	do
		super

		# Respond to UDP discovery requests
		server.answer_discovery_requests

		# Accept TCP connections
		var new_clients = server.accept_clients
		for client in new_clients do
			app.network_info = "{client.socket.address} connected"
			client.writer.serialize level
		end

		# Read messages from connected clients
		var disconnect_clients = new Array[RemoteClient]
		for client in server.clients do
			while client.socket.poll_in do
				var command = client.reader.deserialize

				# Dirty euristic to detect broken connections
				#
				# Such connections should be detected by the underlying `Stream`.
				var eof_msg = "Deserialization Error: Unknown binary object kind `0x00`"
				if client.reader.errors.not_empty and client.reader.errors.first.to_s == eof_msg then
					disconnect_clients.add client
					continue label
				end

				if client.reader.errors.not_empty then print client.reader.errors

				# Apply commands locally and relay them to all clients but the sender
				if command isa Bullet then
					command.create
					server.broadcast(command, except=client)

				else if command isa Character then
					app.network_info = "{command} joined"
					command.create
					server.broadcast(command, except=client)

				else if command isa Command then
					command.apply(self, level)
					server.broadcast(command, except=client)

					if command isa NotifyQuit then
						disconnect_clients.add client
						break
					end
				else
					print "Server received unknown command: {command or else "null"}"
				end
			end
		end label

		# Disconnect clients
		for client in disconnect_clients do
			client.writer.cache.print_stats

			app.network_info = "{client.socket.address} disconnected"
			server.clients.remove client
			client.socket.close
		end
	end

	redef fun share(command) do server.broadcast command

	redef fun new_level
	do
		super

		var msg = new NewLevel(level)
		server.broadcast msg
	end
end

# Client connected to a central remote `server`
class ClientContext
	super NetworkContext

	# Central server
	var server: RemoteServer

	redef fun prepare do level.create_ui

	redef fun update(dt)
	do
		super

		while server.socket.as(not null).poll_in do
			var command = server.reader.deserialize
			if server.reader.errors.not_empty then print server.reader.errors
			if command isa Bullet then
				command.create
			else if command isa Character then
				app.network_info = "{command} joined"
				command.create
			else if command isa Command then
				command.apply(self, level)
			else
				print "Client received command: {command or else "null"}"
			end
		end
	end

	redef fun share(command)
	do
		server.writer.serialize command
		server.socket.as(not null).flush
	end

	redef fun new_level
	do
		var req = new RequestNewLevel
		server.writer.serialize req
		server.socket.as(not null).flush
	end
end

redef class Gun
	redef fun open_fire(shooter)
	do
		var bullet = super
		if shooter isa Character then
			# Share only bullets created by the local player
			var context = app.context
			if context != null then context.share bullet
		end
		return bullet
	end
end

redef class Level
	# Create the UI parts of the world
	#
	# Assumes that `self` is well built, probably deserialized from the central server.
	fun create_ui
	do
		for tower in towers do tower.create_ui
		for character in characters do character.create_ui
		for bullet in bullets do bullet.create_ui
		for turret in turrets do for part in turret.parts do part.create_ui
		for turret in shield_turrets do if turret.active then turret.create_shield
	end
end

# ---
# Commands

# Command sent between clients and server
class Command
	super Serializable

	# Apply this command in the receiver
	fun apply(context: GameContext, level: Level) do end
end

# Update the position of `character`
#
# Pass most values as primitives instead of `Point` so they don't use cached objects.
class UpdatePosition
	super Command
	serialize

	# Target character
	var character: Character

	# X coordinate
	var x: Float

	# Y coordinate
	var y: Float

	# Z coordinate
	var z: Float

	# Gun directional vector, X coordinate
	var ox: Float

	# Gun directional vector, Y coordinate
	var oy: Float

	# Gun directional vector, Z coordinate
	var oz: Float

	redef fun apply(context, level)
	do
		if character == context.player then return

		character.position.x = x
		character.position.y = y
		character.position.z = z
		character.gun.orientation.x = ox
		character.gun.orientation.y = oy
		character.gun.orientation.z = oz

		character.update_ui
	end

	redef fun transient do return true
end

redef class Character
	# Create an `UpdatePosition`
	fun to_position_command: UpdatePosition
	do return new UpdatePosition(self, position.x, position.y, position.z,
	                             gun.orientation.x, gun.orientation.y, gun.orientation.z)

	redef fun destroy
	do
		super

		var context = app.context
		if context != null and self != context.player then
			app.network_info = "{self} has died"
		end
	end

	redef var actor_color is lazy, serialize do
		# Select a color from the index of `self` in the `characters` list
		var colors = once [
			"519623",
			"1c7cdb",
			"e4aa2f",
			"b41c08",
			"8033cc",
			"3bc896",
			"e614a5"]
		var i = level.characters.length
		var hex = colors.modulo(i)

		# Convert from text color to float array
		var parts = [hex.substring(0,2), hex.substring(2,2), hex.substring(4,2)]
		var color = new Array[Float]
		for part in parts do color.add(part.to_hex.to_f/255.0*0.7)
		color.add 1.0

		return color
	end

	# Pretty name of the player
	var name: String = "USER".environ is serialize

	redef fun to_s do return name
end

# Push a new level to clients
class NewLevel
	super Command
	serialize

	# New level to install
	var new_level: Level

	redef fun apply(context, level)
	do
		assert context isa ClientContext # Should only reach clients

		context.level = self.new_level
		context.spawn_player
	end

	redef fun transient do return true
end

# Request a new level from the server
class RequestNewLevel
	super Command
	serialize

	redef fun apply(context, level)
	do
		# Should reach all, but only the server should react
		if context isa ServerContext then
			app.new_level
		end
	end

	redef fun transient do return true
end

# Notify others that `character` has left the game
class NotifyQuit
	super Command
	serialize

	# Leaving character
	var character: Character

	redef fun apply(context, level)
	do
		app.network_info = "{character} has left the game"
	end

	redef fun transient do return true
end

# ---
# UI service `network_info=`

redef class App

	# Display `text` to the user on screen in the network/multi line
	fun network_info=(text: Text)
	do
		print text
		network_text.text = text
		network_info_delay = 8.0
	end
	private var network_text = new TextSprites(font, ui_camera.top_left.offset(32.0, -128.0, 0.0), "") is lazy

	private var network_info_delay = 0.0

	redef fun update(dt)
	do
		if network_info_delay > 0.0 then
			network_info_delay -= dt
			if network_info_delay <= 0.0 then network_text.text = ""
		end

		super
	end
end

redef fun exit(status)
do
	var context = app.context
	if context != null then context.share new NotifyQuit(context.player)

	super
end
