# This file is part of jumpngun by Alexis Laferrière
#
# To the extent possible under law, the person who associated CC0 with
# jumpngun has waived all copyright and related or neighboring rights
# to jumpngun.
#
# You should have received a copy of the CC0 legal code along with this
# work. If not, see http://creativecommons.org/publicdomain/zero/1.0/.

# Simple physics engine
module physics is serialize

import levels

# World gravity, adjusted for nicer effects
fun gravity: Float do return -9.81 * 1.5

redef class Level
	# Live `PhysicsEntity` updated at each frame
	var physic_entities = new BoxedArray[PhysicsEntity]

	# Deadly height on the Y axis
	var death_limit: Float = -100.0

	# Create an explosion at `center` of `force` at 1.0 m
	fun explode(center: Point3d[Float], force: Float)
	do
		for c in physic_entities do
			var d = c.position.dist2(center)+1.0
			var f = force / d

			if f < 4.0 then continue

			c.pushed(center, f)
		end
	end

	redef fun update(dt)
	do
		super

		for e in physic_entities.to_a do
			if e.position.y < death_limit then
				e.destroy
			end
		end
	end
end

# Game entity responding to the physics engine
class PhysicsEntity
	super Boxed3d[Float]
	super Updatable

	# World
	var level: Level

	# Is this entity in freefall?
	var air = false is writable

	# Position in the world
	var position = new Point3d[Float]

	# Movement speed, affects `position`
	var speed = new Point3d[Float]

	# Constant acceleration, affects `speed`
	var acceleration = new Point3d[Float]

	# Create this entity, adding it to world and UI
	fun create
	do
		level.physic_entities.add self
		create_ui
	end

	# Hook to create `self` in the UI
	fun create_ui do end

	# Destroy this entity, removing it from world and UI
	fun destroy do level.physic_entities.remove self

	redef fun update(dt)
	do
		speed.x += acceleration.x*dt
		speed.y += acceleration.y*dt
		speed.z += acceleration.z*dt

		var max_vel = 53.0
		speed.y = speed.y.clamp(-max_vel, max_vel)

		position.x += speed.x*dt
		position.y += speed.y*dt
		position.z += speed.z*dt
	end

	# Push by an explosion at `center` and receiving `relative_force`
	protected fun pushed(center: Point3d[Float], relative_force: Float)
	do
		var dx = position.x - center.x
		var dy = position.y - center.y
		var dz = position.z - center.z
		speed.x += dx*relative_force
		speed.y += dy*relative_force
		speed.z += dz*relative_force

		# Push off the ground
		if not air then speed.y += 2.0
		air = true
	end
end

# Simple entity with the same `dim` on X, Y and Z
abstract class CubicEntity
	super PhysicsEntity

	# Dimension on X, Y and Z
	protected fun dim: Float do return 1.0

	redef fun left do return position.x - dim*0.5
	redef fun right do return position.x + dim*0.5
	redef fun top do return position.y + dim*0.25
	redef fun bottom do return position.y - dim*0.75
	redef fun front do return position.z + dim*0.5
	redef fun back do return position.z - dim*0.5
end
