Simple jetpack centric FPS game

Complete each procedurally generated level by reaching and landing on the gold tower.
Avoid or destroy the red turrets, and destroy shield turrets from within their shields.
Move with WASD, jump with space and shoot with LMB.

# Variants

The module `textured_towers` is bound by default, it replaces the smooth material of towers
by a textured materials.

The optional module `multi` implements multiplayer support with automated discovery of local servers.

# Screenshots

![Starting view](screenshots/start.png)

![Multiplayer](screenshots/multi.png)

# Video

[![Action video](https://img.youtube.com/vi/wPsPPj4XjbU/0.jpg)](https://www.youtube.com/watch?v=wPsPPj4XjbU)

# Artwork

* Original gun and bullet models created by kenney.nl, published under CC0.
* Tower textures created by Nobiax, published under CC0.
* Original turret models created by nazzyc, published under CC BY 3.0.
* Original player model created by Client Bellanger, published under CC BY 3.0.
* Shooting sound created by jonccox under CC BY 3.0.
* Close explosion sound created by qubodup, published under CC0 3.0.
* Step landing sound created by nextmaking, published under CC BY 3.0.
* Original jetpack sound created by Cambra, published under CC BY 3.0.
* Jumping sound created by kfatehi, published under CC 0.
* Victory sound by limetoe, published under CC0.
* Falling sound by jjhouse4, published under CC BY 3.0.
